# ⚡ storm

Dead simple, directory-specific shell command mapping manager.

## Background

Coming from JS/TS development, I'm used defining custom, project-specific scripts using yarn. This can be used, for example, to make the command `yarn deploy` call an Ansible playbook which deploys some files to a server. I find this very convenient and sometimes really missed it when working on projects that don't use yarn. storm aims to do just what yarn scripts do: map shell commands.

## Installation

storm is just a bash script. Download `src/storm`, make it executable (`chmod +x storm`) and add it to your `PATH`.

## Usage

To use storm in your project, you need a file called `storm.conf` in your project directory. In this file you can add your command mappings like this:

```
build=scripts/build.sh
deploy=ansible-playbook -i ansible/inventory.yml ansible/deploy.yml
```

You are then able to use your mappings `storm build` and `storm deploy`.
